# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


#cities = ['Sarajevo', 'Paris', 'London', 'Rome', 'New York', 'Los Angels', 'Barcelona', 'Madrid', 'Las Vegas']


[City, Tag, Tagging, User].each(&:delete_all)

city = City.create({
                name: 'Sarajevo',
                coordinates_x: '43.856259',
                coordinates_y: '18.413076'
            })
city.tags << ['cevapi', 'bascarsija', 'zeljeznicar', 'capital'].map do |tag|
  Tag.where(name: tag).first_or_create!
end

city = City.create({
                name: 'Paris',
                coordinates_x: '48.856614',
                coordinates_y: '2.352222'
            })
city.tags = ['eiffel', 'tower', 'love', 'capital'].map do |tag|
  Tag.where(name: tag).first_or_create!
end

city = City.create({
                name: 'London',
                coordinates_x: '51.507351',
                coordinates_y: '-0.127758'
            })
city.tags = ['queen', 'capital', 'palace'].map do |tag|
  Tag.where(name: tag).first_or_create!
end
city = City.create({
                name: 'Rome',
                coordinates_x: '41.902783',
                coordinates_y: '12.496366'
            })
city.tags = ['coloseum', 'army', 'capital'].map do |tag|
  Tag.where(name: tag).first_or_create!
end
city = City.create({
                name: 'New York',
                coordinates_x: '40.712784',
                coordinates_y: '-74.005941'
            })
city.tags = ['apple', 'statue', 'island'].map do |tag|
  Tag.where(name: tag).first_or_create!
end
city = City.create({
                name: 'Los Angels',
                coordinates_x: '34.052234',
                coordinates_y: '-118.243685'
            })
city.tags = ['bridge', 'movie', 'celebrity'].map do |tag|
  Tag.where(name: tag).first_or_create!
end
city = City.create({
                name: 'Barcelona',
                coordinates_x: '41.385064',
                coordinates_y: '2.173403'
            })
city.tags = ['football', 'club'].map do |tag|
  Tag.where(name: tag).first_or_create!
end
city = City.create({
                name: 'Madrid',
                coordinates_x: '40.416775',
                coordinates_y: '-3.703790'
            })
city.tags = ['capital', 'Real'].map do |tag|
  Tag.where(name: tag).first_or_create!
end
city = City.create({
                name: 'Las Vegas',
                coordinates_x: '36.169941',
                coordinates_y: '-115.139830'
            })
city.tags = ['money', 'casino', 'desert'].map do |tag|
  Tag.where(name: tag).first_or_create!
end

User.create({
                email: 'admin@mail.com',
                password: 'password'
            })


puts 'Seeding completed'

# cities.each do |city|
#   City.where(name: city).tags.create('cevapi')
# end


