class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name
      t.string :coordinates_x
      t.string :coordinates_y

      t.timestamps null: false
    end
  end
end
