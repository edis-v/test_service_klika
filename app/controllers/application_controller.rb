class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  before_action :restrict_access
  helper_method :current_user

  rescue_from CanCan::AccessDenied do |exception|
    render json: {message: exception.message}, status: 403
  end

  def current_user
    authenticate_token
  end

  def restrict_access
    authenticate_token #|| render_unauthorize
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      new_token = AuthToken.find_by(token: token)
      User.find(new_token.user_id) if new_token
    end
  end

  def render_unauthorize
    self.headers['WWW-Authenticate'] = 'Token realm = "Application"'
    render json: {message: 'You Are Not Logged In'}, status: 401
  end

end
