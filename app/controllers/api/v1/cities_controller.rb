class Api::V1::CitiesController < ApplicationController

  before_action :find_city, except: [:index, :create]
  load_and_authorize_resource

  def index
    if params[:query].present?

      @cities = City.where(name: params[:query])

      if @cities.blank?
        tag = Tag.where(name: params[:query]).last
        @cities = tag.cities if tag
      end

    else
      @cities = City.all
    end
    render json: @cities, include: { tags: { only: [:name] } }, status: :ok
  end

  def show
    if @city
      render json: @city, include: { tags: { only: [:name] } }, status: :ok
    else
      render json: @city.errors, status: :not_found
    end
  end

  def create
    @city = City.new(city_params)
    if @city.save
      render json: @city, include: { tags: { only: [:name] } }, status: :created
    else
      render json: @city.errors, status: :bad_request
    end
  end

  def update
    @city.update(city_params)
    if @city.save
      render json: @city, include: { tags: { only: [:name] } }, status: :ok
    else
      render json: @city.errors, status: :bad_request
    end
  end

  def destroy
    @city.destroy
    render nothing: true, status: :no_content
  end

  private

  def city_params
    params.require(:city).permit(:name, :coordinates_x, :coordinates_y, :all_tags)
  end

  def find_city
    @city = City.find(params[:id])
  end

end
