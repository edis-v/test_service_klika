class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    alias_action :create, :read, :update, :destroy, :to => :crud

    if user.new_record?
      puts 'User is guest'
      can [:read], :all
    else
      puts 'User is registered'
      can :crud, :all
    end

  end
end
