class City < ActiveRecord::Base
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings

  validates :name, :coordinates_x, :coordinates_y, presence: true

  def all_tags=(names)
    self.tags = names.split(',').map do |t|
      Tag.where(name: t.strip).first_or_create!
    end
  end

end
