class Tag < ActiveRecord::Base
  has_many :taggings
  has_many :cities, through: :taggings

  validates :name, presence: true

end
