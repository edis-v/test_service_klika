class User < ActiveRecord::Base
  has_secure_password
  has_many :auth_tokens

  validates :password_digest, presence: true
  validates :email, presence: true

end
